import Vue from 'vue';
import Vuex from 'vuex';
import createPriceModule from './modules/price';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default function createStore(config){
  return new Vuex.Store({
    modules: {
      price: createPriceModule(config),
    },
    strict: debug,
  });
};