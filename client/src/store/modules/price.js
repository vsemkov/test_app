import initAPI from '../../api/price';

let api = {};

// initial state
const state = {
  currentShop: {},
  shops: {},
  productId: {},
  price: {},
};

// getters
const getters = {
  currentShop: (state) => {
    return state.currentShop;
  },
  shops: (state) => {
    return state.shops;
  },
  price: (state) => {
    return state.price;
  },
};

// actions
const actions = {
  getListOfShops({commit}) {
    return new Promise((resole, reject) => {
      api.getListOfShops((data)=>{
        commit('setShops', data);
        resole(data);
      }, reject);
    });
  },

  getShop({commit}, settings) {
    return new Promise((resole, reject) => {
      commit('setPrice', {});
      api.getShop(settings, (data) => {
        commit('setCurrentShop', data);
        resole(data);
      }, reject);
    });
  },
  getPriceOfProduct({commit}, settings) {
    commit('setPrice', {});
    return new Promise((resole, reject) => {
      api.getPriceOfProduct(settings, (data) => {
          commit('setPrice', data);
          resole(data);
      }, reject);
    });
  },
  updatePriceOfShop({commit}, settings) {
    return new Promise((resole, reject) => {
      api.updatePriceOfShop(settings, (data) => {
        resole();
      }, reject);
    });
  },
};

// mutations
const mutations = {
  setShops(state, data) {
    state.shops = data;
  },

  setCurrentShop(state, data) {
    state.currentShop = data;
  },

  setPrice(state, data) {
    state.price = data;
  },
};

export default function createPriceModule(config){
  api = initAPI(config);
  return {
    state: state,
    getters: getters,
    actions: actions,
    mutations: mutations,
  };
};
