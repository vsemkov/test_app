import VueRouter from 'vue-router';
import Home from './pages/HomePage.vue';
import Shop from './pages/ShopPage.vue';
import error404 from './pages/404.vue';

const routes = new VueRouter({
  mode: 'history',
  routes: [
    {
      path: '/error',
      component: error404,
    },
    {
      path: '/',
      name: 'home',
      component: Home,
      meta: {},
    },
    {
      path: '/:shopName',
      name: 'Shop',
      component: Shop,
      props: true,
      meta: {},
    },
    {
      path: '*',
      component: error404,
    },
  ],
});

export default routes;
