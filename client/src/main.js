import Vue from 'vue';
import routes from './routes';
import createStore from './store';
import VueRouter from 'vue-router';
import 'bootstrap/dist/css/bootstrap.css';
import config from './config/dev.env';

let store = createStore(config);

Vue.use(VueRouter);
Vue.prototype.$globalBus = new Vue();

Vue.mixin({
  created() {
    this.$services = {};
  },
});

window.app = new Vue({
  el: '#app',
  components: {
  },
  store,
  router: routes,
});
