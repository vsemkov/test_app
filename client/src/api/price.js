import axios from 'axios';

export default function initAPI(config){
    return {
        getListOfShops: (cb, err) => {
        return axios.get(`${config.apihost}/shop`, {headers: {'API': config.apikey}})
        .then((response) => {
                cb(response.data);
            })
            .catch((error) => {
                err(error);
            });
        },

        getShop: (settings, cb, err) => {
            return axios.get(`${config.apihost}/shop/${settings.shopName}`, {headers: {'API': config.apikey}})
            .then((response) => {
                cb(response.data);
            })
            .catch((error) => {
                err(error);
            });
        },

        getPriceOfProduct: (settings, cb, err) => {
        return axios.get(`${config.apihost}/price/${settings.shopName}/${settings.productId}`, {headers: {'API': config.apikey}})
        .then((response) => {
                cb(response.data);
            })
            .catch((error) => {
                err(error);
            });
        },

        updatePriceOfShop: (settings, cb, err) => {
            return axios.post(`${config.apihost}/price/${settings.shopName}`, null, {headers: {'API': config.apikey}})
            .then((response) => {
                cb(response.data);
            })
            .catch((error) => {
                err(error);
            });
        },
    }
};
