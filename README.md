
``` bash
# install dependencies
npm install

# Server config
# config of application, look at /server/config/app.json
{
    DBHost - mongodb coonnect url,
    tempfolder - temp folder for csv parser,
    apikey - API key
}

# config of shop, look at /server/config/shop.json
{
    id - Shop ID
    name - Unique mame of shop
    displayName - Display name of shop
    link - Link to CSV file of price
    type - type of price file,
    charset - charset of price file,,
    fields - parse fields
        price - price field of product
        {
            mapto - field in price file
        }
        reference - reference field (article of product)
        {
            mapto - field in price file
        }
        name - name field of product
        {
            mapto - field in price file
        }
    }
}

# Client config

# config of application, look at /client/src/config/dev.env.js
{
  apihost - server API host,
  apikey - API key
}


# serve at localhost:3000
npm start

# test 
npm test
```