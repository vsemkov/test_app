import chai from 'chai';
import chaiHttp from 'chai-http';
import server from '../../server/app';
import config from '../../server/config/app.test.json';

let should = chai.should();

chai.use(chaiHttp);

describe('APIKey access', () => {

    describe('/GET shops without api key', () => {
        it('it should GET 403 status', (done) => {
            chai.request(server)
                .get('/api/shop')
                .end((err, res) => {
                    res.should.have.status(403);
                    done();
                });
        });
    });

    describe('/GET shops with api key', () => {
        it('it should GET 200 status', (done) => {
            chai.request(server)
                .get('/api/shop')
                .set('api', config.apikey)
                .end((err, res) => {
                    done();
                });
        });
    });
});