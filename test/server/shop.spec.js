import chai from 'chai';
import chaiHttp from 'chai-http';
import server from '../../server/app';
import config from '../../server/config/app.test.json';

let should = chai.should();

chai.use(chaiHttp);

describe('shop API', () => {

    describe('/GET shops', () => {
        it('it should GET all shops', (done) => {
            chai.request(server)
                .get('/api/shop')
                .set('api', config.apikey)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    done();
                });
        });
    });

    describe('/GET shop', () => {
        it('it should GET shop by name', (done) => {
            chai.request(server)
                .get('/api/shop/shop_2')
                .set('api', config.apikey)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    done();
                });
        });
    });
});