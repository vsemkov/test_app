import chai from 'chai';
import Shop from '../../server/models/shops';
import shopConfig from '../../server/config/shops.test.json';

let should = chai.should();

let shop = new Shop(shopConfig);

describe('Test Shop config', () => {
    describe('List all shops', () => {
        it('it should array', (done) => {
            shop.items.should.be.an('array');
            done();
        });
    });

    describe('Get Shop info by ID', () => {
        it('it should be Shop properties: name:String, link:String, fields:Array', (done) => {
            let shopItem = shop.findById(1);
            shopItem.should.be.an('object').to.be.have.all.keys('id', 'name', 'link', 'fields', 'charset', 'displayName','type');
            done();
        });
    });

    describe('Get Shop info by name', () => {
        it('it should be Shop properties: name:String, link:String, fields:Array', (done) => {
            let shopItem = shop.findByName("shop_2");
            shopItem.should.be.an('object').to.be.have.all.keys('id', 'name', 'link', 'fields', 'charset', 'displayName','type');
            done();
        });
    });
});