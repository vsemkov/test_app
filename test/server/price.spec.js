import chai from 'chai';
import chaiHttp from 'chai-http';
import server from '../../server/app';
import config from '../../server/config/app.test.json';

let should = chai.should();

chai.use(chaiHttp);

describe('Price API', () => {
    describe('/GET /api/price/:shopName/:productId', () => {
        it('it should GET price by shopName', (done) => {
            chai.request(server)
            .get('/api/price/shop_1/1')
            .set('api', config.apikey)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                done();
            });
        });
    });

    /* describe('/POST /:shopName', () => {
        it('it should GET price by shopName', (done) => {
            chai.request(server)
            .get('/shop_1/1')
            .set('api', config.apikey)
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('Object');
                done();
            });
        });
    });*/
});
