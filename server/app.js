import express from 'express';
import path from 'path';
import logger from 'morgan';
import bodyParser from 'body-parser';
import webpack from 'webpack';
import webpackMiddleware from 'webpack-dev-middleware';
import webpackConfig from '../webpack.config.js';
import config from './config/app';
import mongoose from 'mongoose';

mongoose.connect(config.DBHost, {useNewUrlParser: true});
let db = mongoose.connection;

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log("db connected");
});

let api = require('./routes/api');
let app = express();

if (process.env.NODE_ENV !== 'test') {
  app.use(webpackMiddleware(webpack(webpackConfig)));
}

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use('/api', function(req, res, next) {
  if (req.headers['api'] === config.apikey) {
    res.status(200);
    res.setHeader('Content-Type', 'application/json');
    next();
  } else {
    res.status(403).end();
  }
}, api);

app.use('/', function(req, res, next) {
  res.render('index');
  next();
});

// error handler
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.send(JSON.stringify(err));
});

export default app;
