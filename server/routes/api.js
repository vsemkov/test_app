import express from 'express';
import Price from './../models/price';
import shopConfig from './../config/shops.json';
import Shop from './../models/shops';
import PriceLoader from '../services/priceLoader';

let router = express.Router();
let shops = new Shop(shopConfig);

router.post('price/:shopName', function(req, res, next) {
  let shop = shops.findByName(req.params.shopName);
  if (shop && shop.link) {
    let priceLoader = new PriceLoader(shop);
    priceLoader.downloadAndParse()
      .then((data) => {
        Price.deleteMany({shopId: shop.id}, (err) => {
          if (err) return next(err);
          Price.collection.insertMany(data, (err, result) => {
            if (err) return next(err);
           res.send({done: true});
          });
        });
      }).catch((e)=>{
        next(e);
      });
  } else {
    let err = new Error('Shop not Found');
    err.status = 404;
    next(err);
  }
});

router.get('/shop', function(req, res, next) {
  res.send(JSON.stringify(shops.items));
});

router.get('/shop/:shopName', function(req, res, next) {
  let shop = shops.findByName(req.params.shopName);
  if (shop) {
    res.send(JSON.stringify(shop));
  } else {
    let err = new Error('Shop not Found');
    err.status = 404;
    next(err);
  }
});

router.get('/price/:shopName/:productId', function(req, res, next) {
  let shop = shops.findByName(req.params.shopName);
  let productId = Number(req.params.productId);
  Price.find({shopId: shop.id, productId: productId}, (err, result)=>{
    if (err) {
      return next(err);
    }
    res.send(JSON.stringify(result));
  });
});

module.exports = router;
