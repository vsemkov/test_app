/**
* Shop class
*/
class Shop {

    /**
    * constructor
    * @param {Object} config - Config object 
    */
    constructor(config) {
        this.shopConfig = config;
    }
    
    /**
    * Getter List of shops
    * @return {Array}
    */
    get items() {
        return this.shopConfig.shops;
    }

    /**
    * Find Shop by name
    * params:
    * @param {String} name - Shop name
    * @return {Object}
    */
    findByName(name) {
        let result;
        this.shopConfig.shops.forEach(function(element) {
            if (element.name == name) {
                result = element;
                return;
            }
        });
        return result;
    }

     /**
     * Find Shop by Id
     * params:
     * @param {Number} id - Shop Id
     * @return {Object}
     */
    findById(id) {
        let result;
        this.shopConfig.shops.forEach(function(element) {
            if (element.id == id) {
                result = element;
                return;
            }
        });
        return result;
    }
}

export default Shop;
