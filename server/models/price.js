let mongoose = require('mongoose');
let Schema = mongoose.Schema;

let PriceSchema = new Schema(
    {
        productId: {type: Number},
        name: {type: String, required: true},
        reference: {type: String, required: false},
        price: {type: Number, required: true},
        shopId: {type: Number, required: true},
        createdAt: {type: Date, required: false},
    },
    {
        versionKey: false,
    }
);

module.exports = mongoose.model('price', PriceSchema);
