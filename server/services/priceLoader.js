import config from './../config/app';
import Excel from 'exceljs';
import http from 'http';
import fs from 'fs';
import iconv from 'iconv-lite';
/**
* PriceDownloader class
*/
class PriceLoader {
    /**
    * constructor
    * params:
    * @param {Object} shop - shop configuration
    */
    constructor(shop) {
        this.shop = shop;
    }

    /**
     * Download and parse file
     * params:
     * @return {Promise}
     */
    downloadAndParse() {
        let fileName = `${config.tempfolder}/tempfile.csv`;
        return new Promise((resolve, reject) => { 
            this.downloadFile(fileName).then(() => {
                this.parseFile(fileName).then((result) => {
                    resolve(result);
                });
            }).catch((err) => {
                reject(err);
                console.log('error:', err);
            });
        });
    }

    /**
     * Parse file
     * params:
     * @param {String} filename - download url
     * @return {Promise}
     */
    parseFile(filename) {
        let ctx=this;
        return new Promise((resolve, reject) => {
            let workbook = new Excel.Workbook();
            workbook.csv.readFile(filename, {
                delimiter: ';',
            })
            .then(function(worksheet) {
                let fields = ctx.shop.fields;
                let data = [];
                worksheet.getRow(1).eachCell(function(cell, colNumber) {
                    Object.keys(fields).forEach((key) => {
                        if (cell.value == fields[key].mapto) {
                            fields[key].columnIndex = colNumber;
                        }
                    });
                });
                //remove first header
                worksheet.spliceRows(1, 1);

                // Iterate over all rows (including empty rows) in a worksheet
                worksheet.eachRow({includeEmpty: false}, function(row, rowNumber) {
                    let record = {
                        productId: rowNumber,
                        shopId: ctx.shop.id,
                        createdAt: new Date(),
                        reference: '',
                    };
                    Object.keys(fields).forEach((key) => {
                        let field = fields[key];
                        record[key]=row.getCell(field.columnIndex).value;
                    });
                    data.push(record);
                });
                ctx.deleteFile(filename);
                resolve(data);
            });
        });
    }

    /**
     * Delete file
     * params:
     * @param {String} filename - download url
     */
    deleteFile(filename) {
        fs.unlink(filename, () => {}); // Delete temp file
    }

    /**
     * Download file from url
     * params:
     * @param {String} dest - temp file destination
     * @return {Promise}
     */
    downloadFile(dest) {
        return new Promise((resolve, reject) => {
            const file = fs.createWriteStream(dest, {flags: 'wx', encoding: 'binary'});
            const request = http.get(this.shop.link, (response) => {
                if (response.statusCode === 200) {
                    response.pipe(iconv.decodeStream(this.shop.charset))
                    .pipe(iconv.encodeStream('utf8'))
                    .pipe(file);
                } else {
                    file.close();
                    this.deleteFile(dest);
                    reject(`Server responded with ${response.statusCode}: ${response.statusMessage}`);
                }
            });

            request.on('error', (err) => {
                file.close();
                this.deleteFile(dest);
                reject(err.message);
            });

            file.on('finish', () => {
                resolve();
            });

            file.on('error', (err) => {
                file.close();
                if (err.code === 'EEXIST') {
                    reject('File already exists');
                } else {
                    this.deleteFile(dest);
                    reject(err.message);
                }
            });
        });
    }
}

export default PriceLoader;
